This module changes your image gallery administration page to let you apply 
role-based permissions to each gallery, and to give each gallery individual 
moderators.

Moderators automatically get all privileges on all posts in that gallery,
including edit and delete. 